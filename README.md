# Triple Higgs studies

The Jupyter notebooks can be run via the NAF JupyterHub instance. Instructions can be found here:
https://confluence.desy.de/display/IS/Jupyter-on-NAF_135774237.html

You should set up your own kernel, as described here:

https://confluence.desy.de/display/IS/Jupyter-on-NAF_135774237.html#JupyteronNAF-UsingPythonVirtualenvs

In this kernel, you need to install the extra packages needed for this project:

```
pip install --upgrade pip

pip install mplhep, uproot, awkward, pandas, tensorflow, sklearn
```
=======


# Reading material:

Overview of ATLAS HHH 6b (internal, sensitive material!):
https://indico.cern.ch/event/1365211/contributions/5744349/attachments/2779341/4843989/HHH_1st_EBMeeting.pdf

Introduction to transformers:
https://arxiv.org/pdf/2304.10557

Particle transformer for jet tagging (includes example of interactions input):
https://arxiv.org/pdf/2202.03772
