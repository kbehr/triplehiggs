import uproot
import awkward as ak



def readntuple_pre(pathprefix, samplename=None, cuts=None):

    dict_lumis = {"20a": 36.64674, "20d": 44.6306, "20e": 58.7916}  # Don't know if values are correct...
    
    branches = ['jets_E', 'jets_pt', 'jets_eta', 'jets_phi',
               'mH1', 'mH3', 'mH2', 'mHHH',
               'weight', 'isSR', 'nBTags']

    
    arrs = []
    for camp in ["20a", "20d", "20e"]:
        with uproot.open(f"{pathprefix}_MC{camp}/ntuple.root:HHHNtuple") as up:
            arr = up.arrays(branches, cut=cuts, library='ak')
            arr['totalWeight'] = arr['weight'] * dict_lumis[camp]
            if samplename is not None:
                arr['Sample'] = [samplename] * len(arr)
            arrs.append(arr)
            
    arr = ak.concatenate(arrs)
    arr['normalisedWeight'] = arr['totalWeight'] / sum(arr['totalWeight'])
    return arr

def readntuple_post(path, samplename=None, cuts=None):

    branches = ['njets', 'eventWeight',
                'mH1', 'mH2', 'mH3', 'pTH1', 'pTH2', 'pTH3',
               'mHHH', 'pTHHH',
               'skewnessPt', 'kurtosisPt']
    
    with uproot.open(f"{path}:tree") as up:
        arr = up.arrays(branches, cut=cuts, library='pd')
        arr['eventWeightNorm'] = arr['eventWeight'] / sum(arr['eventWeight'])
       
    return arr